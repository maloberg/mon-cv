document.addEventListener('DOMContentLoaded', function()
{
    const domCloseButton = document.getElementById('cmd-nav');
    const domHiddenClass = document.getElementById('menu-nav');
    const domNavLi = document.querySelectorAll('nav.menu-nav ol li');

    console.log(domNavLi);
    domCloseButton.addEventListener('click', function(){
        let isMenuHidden = domHiddenClass.classList.contains('hidden')
        
        if(isMenuHidden )
        {
            domHiddenClass.classList.remove('hidden');
            domCloseButton.classList.add('close');
        }

        else
        {
            domHiddenClass.classList.add('hidden');
            domCloseButton.classList.remove('close');
        }
    })
    domNavLi.forEach(item =>
    {
        item.addEventListener('click',function()
        {
            domHiddenClass.classList.add('hidden');
            domCloseButton.classList.remove('close');
        })
    })
})
